from __future__ import annotations

from django.contrib.auth.hashers import check_password
from django.utils.crypto import get_random_string
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from main.api.general import checkToken
from main.models import Device
from main.models import DeviceUser
from main.models import User
from main.serializer import RegisterSerializer
from main.models import BlockedUser
from main.api.general import checkUserBlock


class UserCreate(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = RegisterSerializer


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


@api_view(['POST'])
def login(request):
    if request.method == 'POST':
        data = request.data
        username = data['username']
        password = data['password']
        ip = get_client_ip(request)
        devicename = data['devicename']
        deviceid = data['deviceid']
        try:
            user = User.objects.get(username=username)
        except Exception:
            return Response(
                'Username/Password Wrong',
                status=status.HTTP_400_BAD_REQUEST,
            )
        if not check_password(password, user.password):
            return Response(
                'Username/Password Wrong',
                status=status.HTTP_400_BAD_REQUEST,
            )
        if deviceid != '':
            try:
                device = Device.objects.get(id=deviceid)
            except Exception:
                return Response(
                    'Device with ID not Found',
                    status=status.HTTP_400_BAD_REQUEST,
                )
        else:
            device = Device.objects.create(
                name=devicename,
                ip=ip,
            )
        try:
            deviceuser = DeviceUser.objcets.filter(
                user=user,
            ).filter(device=device).first()
            token = get_random_string(length=100)
            deviceuser.token = token
            deviceuser.save()
        except Exception:
            deviceuser = DeviceUser.objects.create(
                user=user,
                device=device,
                token=get_random_string(length=100),
            )
        data = {
            'token': deviceuser.token,
            'deviceid': device.id,
            'salt': user.salt,
            'private_key_encrypted': user.private_key_encrypted,
        }

        return Response(data)


@api_view(['GET'])
def me(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    user_json = {
        'username': user.username,
        'visibility': user.visibility,
    }
    return Response(user_json)


@api_view(['POST'])
def blockUser(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    username = data['username']
    blocked = User.objects.filter(username=username).first()
    BlockedUser.objects.create(blocking_user=user, blocked_user=blocked)
    return Response("Blocked")


@api_view(['GET'])
def blockedUser(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    blocked = BlockedUser.objects.filter(blocking_user=user)
    block_json = []
    for block in blocked:
        bjson = {
            'name': block.blocked_user.username,
        }
        block_json.append(bjson)
    return Response(block_json)



@api_view(['POST'])
def unblockUser(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    blocked = data['username']
    blocked = BlockedUser.objects.filter(blocked_user__username=blocked).filter(blocking_user=user).first()
    blocked.delete()
    return Response("Unblocked")
