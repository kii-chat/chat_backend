from __future__ import annotations

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from main.api.general import checkChatAuth
from main.api.general import checkToken
from main.api.general import checkChatBlock
from main.api.general import checkUserBlock
from main.models import Chat
from main.models import ChatUser
from main.models import InviteCode
from main.models import User


def addUsersToGrp(usernameList, inviteCodes, chat, user):
    for username in usernameList:
        try:
            u = User.objects.filter(username=username['username']).exclude(
                visibility='pr',
            ).first()
            if not checkChatBlock(user, chat):
                if not checkUserBlock(user, u):
                    ChatUser.objects.create(chat=chat, user=u, role=username['role'])
        except Exception:
            print('Invalid Username')
    for code in inviteCodes:
        try:
            invite_code = InviteCode.objects.filter(
                code=code['code'],
            ).filter(used=False).first()
            u = invite_code.user
            if not checkChatBlock(user, chat):
                if not checkUserBlock(user, u):
                    ChatUser.objects.create(
                        chat=chat, user=u, role=code['role'],
                    )
        except Exception:
            print('Invalid Invite Code')


@api_view(['POST'])
def createGroup(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    name = data['name']
    usernameList = data['users']
    inviteCodes = data['codes']
    chat = Chat.objects.create(name=name)
    ChatUser.objects.create(chat=chat, user=user, role='ad')
    addUsersToGrp(usernameList, inviteCodes, chat, user)
    return Response('Created')


@api_view(['POST'])
def addUsersToGroup(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    chatid = data['chatid']
    chat = Chat.objects.get(id=chatid)
    unauthorized = checkChatAuth(user, chat)
    if unauthorized:
        return Response(
            'Not Authorized for the Chat',
            status=status.HTTP_403_FORBIDDEN,
        )
    usernameList = data['users']
    inviteCodes = data['codes']
    addUsersToGrp(usernameList, inviteCodes, chat, user)
    return Response('Created')


@api_view(['POST'])
def changeUserRoleInChat(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    chatid = data['chatid']
    chat = Chat.objects.get(id=chatid)
    unauthorized = checkChatAuth(user, chat)
    if unauthorized:
        return Response(
            'Not Authorized for the Chat',
            status=status.HTTP_403_FORBIDDEN,
        )
    changeUserId = data['userid']
    role = data['role']
    chatuser = ChatUser.objects.filter(chat=chat).filter(user=user).first()
    changeUser = User.objects.get(id=changeUserId)
    changeChatUser = ChatUser.objects.filter(
        chat=chat,
    ).filter(user=changeUser).first()
    if chatuser.role != 'ad':
        return Response(
            'Role not Authorized',
            status=status.HTTP_403_FORBIDDEN,
        )
    changeChatUser.role = role
    changeChatUser.save()
    return Response('Updated')
