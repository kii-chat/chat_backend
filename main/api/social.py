from __future__ import annotations

import string

from django.utils.crypto import get_random_string
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from main.api.general import checkToken
from main.api.general import checkUserBlock
from main.models import Chat
from main.models import ChatUser
from main.models import InviteCode
from main.models import User


def createChat(user1, user2):
    if not checkUserBlock(user1, user2):
        if not checkUserBlock(user2, user1):
            chat = Chat.objects.create()
            ChatUser.objects.create(chat=chat, user=user1, role='ad')
            if user2.username != user1.username:
                ChatUser.objects.create(chat=chat, user=user2, role='ad')


@api_view(['POST'])
def createChatWithUsername(request):
    unauthorized, own_user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    username = data['username']
    try:
        other_user = User.objects.get(username=username)
    except Exception:
        return Response("User does not exists")
    if other_user.visibility == 'pr':
        return Response(
            'User Visibility is private',
            status=status.HTTP_403_FORBIDDEN,
        )
    createChat(own_user, other_user)
    return Response('Created')


@api_view(['POST'])
def createChatWithInviteCode(request):
    unauthorized, own_user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    code = data['code']
    try:
        invite_code = InviteCode.objects.filter(
            code=code,
        ).filter(used=False).first()
    except Exception:
        return Response('Code is not valid', status=status.HTTP_403_FORBIDDEN)
    other_user = invite_code.user
    createChat(own_user, other_user)
    invite_code.used = True
    invite_code.save()
    return Response('Created')


@api_view(['GET'])
def getPublicUsers(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    users = User.objects.filter(visibility='pu')
    users_json = []
    for user in users:
        users_json.append({
            'username': user.username,
        })
    return Response(users_json)


@api_view(['POST'])
def setVisibility(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    visibility = data['visibility']
    user.visibility = visibility
    user.save()
    return Response('Changed')


def createNewInviteCode():
    code = get_random_string(
        length=8, allowed_chars=string.ascii_uppercase + string.digits,
    )
    if len(InviteCode.objects.filter(code=code)) > 0:
        return createNewInviteCode()
    return code


@api_view(['GET'])
def createInviteCode(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    codecode = createNewInviteCode()
    code = InviteCode.objects.create(user=user, code=codecode, used=False)
    json = {
        'code': code.code,
    }
    return Response(json)


@api_view(['POST'])
def deleteInviteCode(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    code = data['code']
    InviteCode.objects.filter(user=user).filter(code=code).first().delete()
    return Response('Deleted')


@api_view(['GET'])
def getInviteCodes(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    codes = InviteCode.objects.filter(user=user).filter(used=False)
    json = []
    for code in codes:
        json.append(code.code)
    return Response(json)
