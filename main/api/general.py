from __future__ import annotations

from main.models import ChatUser
from main.models import DeviceUser
from main.models import BlockedChat
from main.models import BlockedUser


def checkToken(request):
    header = request.headers
    token = header['authorization']
    try:
        user = DeviceUser.objects.get(token=token).user
        if user is None:
            return True, None
        return False, user
    except Exception:
        return True, None


def checkChatAuth(user, chat):
    try:
        cu = ChatUser.objects.filter(user=user).filter(chat=chat).first()
        if cu is None:
            return True
        return False
    except Exception:
        return True


def checkUserBlock(own_user, check_user):
    try:
        bu = BlockedUser.objects.filter(blocked_user=own_user).filter(blocking_user=check_user).first()
        if bu is None:
            return False
        return True
    except Exception:
        return False


def checkChatBlock(user, chat):
    try:
        bc = BlockedChat.objects.filter(user=user).filter(chat=chat).first()
        if bc is None:
            return False
        return True
    except Exception:
        return False
