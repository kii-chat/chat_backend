from __future__ import annotations
from django.db.models import Q
import datetime

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from main.api.general import checkChatAuth
from main.api.general import checkToken
from main.models import Chat
from main.models import ChatUser
from main.models import Message
from main.models import MessageUser
from main.models import MessageUserText
from main.models import User
from main.models import BlockedChat


@api_view(['GET'])
def getAllPublicKeysFromChat(request):
    unauthorized, own_user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.GET
    chatid = data['chatid']
    chat = Chat.objects.get(id=chatid)
    unauthorized = checkChatAuth(own_user, chat)
    if unauthorized:
        return Response(
            'Not Authorized for the Chat',
            status=status.HTTP_403_FORBIDDEN,
        )
    chat_users = ChatUser.objects.filter(chat=chat)
    public_keys = []
    for chat_user in chat_users:
        public_keys.append({
            'public_key': chat_user.user.public_key,
            'user_id': chat_user.user.id,
        })
    return Response(public_keys)


@api_view(['POST'])
def sendTextMessage(request):
    unauthorized, own_user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    chatid = data['chatid']
    chat = Chat.objects.get(id=chatid)
    unauthorized = checkChatAuth(own_user, chat)
    if unauthorized:
        return Response(
            'Not Authorized for the Chat',
            status=status.HTTP_403_FORBIDDEN,
        )
    user_messages = data['user_messages']
    referenced_message_pk = data.get('referenced_message_pk')
    if referenced_message_pk is not None:
        referenced_message = Message.objects.filter(id=referenced_message_pk).first()
        message = Message.objects.create(sender=own_user, chat=chat, referenced_message=referenced_message)
    else:
        message = Message.objects.create(sender=own_user, chat=chat)
    for obj in user_messages:
        receiver = User.objects.get(id=obj['user_id'])
        msg_user = MessageUser.objects.create(
            message=message, receiver=receiver,
        )
        if receiver == own_user:
            msg_user.read_at = datetime.datetime.now()
        MessageUserText.objects.create(
            message_user=msg_user, encrypted_message=obj['encrypted_message'],
        )
    return Response('Send')


@api_view(['GET'])
def getChatsForUser(request):
    unauthorized, own_user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    own_chat_users = ChatUser.objects.filter(user=own_user)
    chats = []
    empty_chats = []
    for own_chat_user in own_chat_users:
        chat = own_chat_user.chat
        unauthorized = checkChatAuth(own_user, chat)
        if unauthorized:
            return Response(
                'Not Authorized for the Chat',
                status=status.HTTP_403_FORBIDDEN,
            )
        name = getChatName(chat, own_user)
        last_message = Message.objects.filter(chat=chat).last()
        if last_message is not None:
            last_message_user = MessageUser.objects.filter(
                message=last_message,
            ).filter(Q(receiver=own_user) | Q(message__sender=own_user)).first()
            last_message_user_text = MessageUserText.objects.filter(
                message_user=last_message_user,
            ).first()
            unread_count = MessageUser.objects.filter(message__chat=chat).filter(read_at__isnull=True).filter(receiver=own_user).count()
            chats.append({
                'chat_id': chat.id,
                'chat_name': name,
                'last_message': {
                    'sender': last_message.sender.username,
                    'timestamp': last_message.timestamp,
                    'encrypted_message': last_message_user_text.encrypted_message,  # noqa
                },
                'unread_count': unread_count,
            })
        else:
            empty_chats.append({
                'chat_id': chat.id,
                'chat_name': name,
            })
    for chat in empty_chats:
        chats.append(chat)
    return Response(chats)


@api_view(['GET'])
def getMessagesFromChat(request):
    unauthorized, own_user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.GET
    chatid = data['chatid']
    chat = Chat.objects.get(id=chatid)
    unauthorized = checkChatAuth(own_user, chat)
    if unauthorized:
        return Response(
            'Not Authorized for the Chat',
            status=status.HTTP_403_FORBIDDEN,
        )
    page = int(data['page'])
    pagesize = int(data['pagesize'])
    messages_json = []
    try:
        messages = Message.objects.filter(
            chat=chat,
        ).order_by('-timestamp')[page*pagesize:page*pagesize+pagesize]
    except Exception:
        print(Exception)
        print('No Message')
    if len(messages) > 0:
        for message in messages:
            message_user = MessageUser.objects.filter(
                message=message,
            ).filter(receiver=own_user).first()
            message_user_text = MessageUserText.objects.get(
                message_user=message_user,
            )
            json = {
                'sender': message.sender.username,
                'timestamp': message.timestamp,
                'encrypted_message': message_user_text.encrypted_message,
                'message_user_pk': message_user.pk,
                'message_pk': message.pk,
                'read_at': message_user.read_at,
            }
            if message.referenced_message is not None:
                referenced_message_user = message_user = MessageUser.objects.filter(
                    message=message.referenced_message
                ).filter(receiver=own_user).first()
                referenced_message_user_text = MessageUserText.objects.get(
                    message_user=referenced_message_user,
                )
                json['referenced_message'] = {
                        'sender': message.referenced_message.sender.username,
                        'timestamp': message.referenced_message.timestamp,
                        'encrypted_message': referenced_message_user_text.encrypted_message,
                    }
            messages_json.append(json)
    return Response(messages_json)


@api_view(['GET'])
def getInfosFromChat(request):
    unauthorized, own_user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.GET
    chatid = data['chatid']
    chat = Chat.objects.get(id=chatid)
    unauthorized = checkChatAuth(own_user, chat)
    if unauthorized:
        return Response(
            'Not Authorized for the Chat',
            status=status.HTTP_403_FORBIDDEN,
        )
    chat.name = getChatName(chat, own_user)
    info_json = {
        'name': chat.name,
        'description': chat.description,
    }
    return Response(info_json)


def getChatName(chat, own_user):
    if chat.name is None:
        chat_users = ChatUser.objects.filter(
            chat=chat,
        ).exclude(user=own_user)
        if len(chat_users) == 1:
            name = chat_users.first().user.username
        elif len(chat_users) == 0:
            name = 'Own Chat'
        else:
            name = 'Chat'
    else:
        name = chat.name
    return name


@api_view(['POST'])
def updateChat(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    chatid = data['chatid']
    chat = Chat.objects.filter(id=chatid).first()
    unauthorized = checkChatAuth(user, chat)
    if unauthorized:
        return Response(
            'Not Authorized for the Chat',
            status=status.HTTP_403_FORBIDDEN,
        )
    name = data['name']
    description = data['description']
    chat.name = name
    chat.description = description
    chat.save()
    return Response('Updated')


@api_view(['POST'])
def setRead(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    chatid = data['chatid']
    chat = Chat.objects.filter(id=chatid).first()
    unauthorized = checkChatAuth(user, chat)
    if unauthorized:
        return Response(
            'Not Authorized for the Chat',
            status=status.HTTP_403_FORBIDDEN,
        )
    message_user_pk = data['message_user_pk']
    read_at = data['read_at']
    message_user = MessageUser.objects.get(pk=message_user_pk)
    message_user.read_at = read_at
    message_user.save()
    return Response('Set Read')


@api_view(['POST'])
def blockChat(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    blocked_id = data['chat_id']
    blocked = Chat.objects.filter(id=blocked_id).first()
    BlockedChat.objects.create(user=user, chat=blocked)
    leaveChat(request)


@api_view(['POST'])
def leaveChat(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    leaved_id = data['chat_id']
    chat = Chat.objects.filter(id=leaved_id).first()
    chat_user = ChatUser.objects.filter(user=user).filter(chat=chat)
    chat_user.delete()
    return Response("Leaved")


@api_view(['GET'])
def blockedChat(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    blocked = BlockedChat.objects.filter(blocking_user=user)
    block_json = []
    for block in blocked:
        bjson = {
            'name': block.chat.name,
        }
        block_json.append(bjson)
    return Response(block_json)


@api_view(['POST'])
def unblockChat(request):
    unauthorized, user = checkToken(request)
    if unauthorized:
        return Response('Not Authorized', status=status.HTTP_401_UNAUTHORIZED)
    data = request.data
    blocked_id = data['chat_id']
    blocked = BlockedChat.objects.filter(id=blocked_id).filter(user=user).first()
    blocked.delete()
    return Response("Unblocked")
