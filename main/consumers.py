from __future__ import annotations

from channels.db import database_sync_to_async
from djangochannelsrestframework.consumers import AsyncAPIConsumer
from djangochannelsrestframework.observer import model_observer
from djangochannelsrestframework.observer.generics import action
from djangochannelsrestframework.observer.generics import ObserverModelInstanceMixin  # noqa

from main.models import DeviceUser
from main.models import MessageUser
from main.models import ChatUser


class MessageUserConsumer(AsyncAPIConsumer):
    # This class MUST subclass `AsyncAPIConsumer` to use `@model_observer`

    def get_message(self, pk):
        message_user = MessageUser.objects.get(pk=pk)
        encrypted_message = message_user.MessageUser_Text.first().encrypted_message
        message = message_user.message
        chat = message.chat.id
        sender = message.sender.username
        ret = {
            'encrypted_message': encrypted_message,
            'chat': chat,
            'sender': sender,
            'message_user_pk': message_user.pk,
        }
        return ret

    def get_user_from_token(self, token):
        try:
            user = DeviceUser.objects.get(token=token).user
            return False, user.pk
        except Exception:
            return True, None

    @model_observer(MessageUser)
    async def message_user_change_handler(
            self,
            message,
            observer=None,
            action=None,
            subscribing_request_ids=[],
            **kwargs,
    ):
        # due to not being able to make DB QUERIES when selecting a group
        # maybe do an extra check here to be sure the user has permission
        # send activity to your frontend
        for request_id in subscribing_request_ids:
            # we can send a seperate message for each subscribing request
            # this lets ws clients rout these messages.
            custom_message = await database_sync_to_async(self.get_message)(message['pk'])
            await self.send_json(dict(body=custom_message, action=action, request_id=request_id))
        # note if we do not pass `request_id` to the `subscribe` method
        # then `subscribing_request_ids` will be and empty list.

    @message_user_change_handler.groups_for_signal
    def message_user_change_handler(self, instance: MessageUser, **kwargs):
        # this block of code is called very often *DO NOT make DB QUERIES HERE*
        yield f'-user__{instance.receiver.pk}'
        yield f'-pk__{instance.pk}'

    @message_user_change_handler.groups_for_consumer
    def message_user_change_handler(self, user_pk=None, message_user_pk=None, **kwargs):
        # This is called when you subscribe/unsubscribe
        if user_pk is not None:
            yield f'-user__{user_pk}'
        if message_user_pk is not None:
            yield f'-pk__{message_user_pk}'

    @action()
    async def subscribe_to_message_user_from_user(self, token, request_id, **kwargs):
        error, user_pk = await database_sync_to_async(self.get_user_from_token)(token)
        if not error:
            await self.message_user_change_handler.subscribe(user_pk=user_pk, request_id=request_id)

    @action()
    async def subscribe_to_message_user(self, message_user_pk, request_id, **kwargs):
        await self.message_user_change_handler.subscribe(message_user_pk=message_user_pk, request_id=request_id)


class ChatUserConsumer(AsyncAPIConsumer):
    # This class MUST subclass `AsyncAPIConsumer` to use `@model_observer`

    def get_user_from_token(self, token):
        try:
            user = DeviceUser.objects.get(token=token).user
            return False, user.pk
        except Exception:
            return True, None

    @model_observer(ChatUser)
    async def chat_user_change_handler(
            self,
            message,
            observer=None,
            action=None,
            subscribing_request_ids=[],
            **kwargs,
    ):
        # due to not being able to make DB QUERIES when selecting a group
        # maybe do an extra check here to be sure the user has permission
        # send activity to your frontend
        for request_id in subscribing_request_ids:
            # we can send a seperate message for each subscribing request
            # this lets ws clients rout these messages.
            custom_message = {
                "newChat": "True",
            }
            await self.send_json(dict(body=custom_message, action=action, request_id=request_id))
        # note if we do not pass `request_id` to the `subscribe` method
        # then `subscribing_request_ids` will be and empty list.

    @chat_user_change_handler.groups_for_signal
    def chat_user_change_handler(self, instance: ChatUser, **kwargs):
        # this block of code is called very often *DO NOT make DB QUERIES HERE*
        yield f'-user__{instance.user.pk}'
        yield f'-pk__{instance.pk}'

    @chat_user_change_handler.groups_for_consumer
    def chat_user_change_handler(self, user_pk=None, chat_user_pk=None, **kwargs):
        # This is called when you subscribe/unsubscribe
        if user_pk is not None:
            yield f'-user__{user_pk}'
        if chat_user_pk is not None:
            yield f'-pk__{chat_user_pk}'

    @action()
    async def subscribe_to_chat_user_from_user(self, token, request_id, **kwargs):
        error, user_pk = await database_sync_to_async(self.get_user_from_token)(token)
        if not error:
            await self.chat_user_change_handler.subscribe(user_pk=user_pk, request_id=request_id)

    @action()
    async def subscribe_to_chat_user(self, chat_user_pk, request_id, **kwargs):
        await self.chat_user_change_handler.subscribe(chat_user_pk=chat_user_pk, request_id=request_id)
