from __future__ import annotations

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(
        self, username, password,
        is_staff, is_superuser, **extra_fields,
    ):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        user = self.model(
            username=username,
            is_staff=is_staff, is_active=True,
            is_superuser=is_superuser,
            date_joined=now, **extra_fields,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        return self._create_user(
            username, password, False, False,
            **extra_fields,
        )

    def create_superuser(self, username, password, **extra_fields):
        return self._create_user(
            username, password, True, True,
            **extra_fields,
        )


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField('username', max_length=30, unique=True)
    private_key_encrypted = models.TextField('Encrypted Private Key')
    public_key = models.TextField('Public Key')
    salt = models.TextField('Salt')
    date_joined = models.DateTimeField('date joined', auto_now_add=True)
    is_active = models.BooleanField('active', default=True)
    is_staff = models.BooleanField('is_staff', default=False)

    class UserVisibility(models.TextChoices):
        PRIVATE = 'pr', 'private'
        UNLISTED = 'un', 'unlisted'
        PUBLIC = 'pu', 'public'

    visibility = models.CharField(
        max_length=2,
        choices=UserVisibility.choices,
        default=UserVisibility.UNLISTED,
    )

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        return f'{self.username}'

    def get_short_name(self):
        return f'{self.username}'

    def __str__(self):
        return f'{self.username}'

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'User'


class DeviceUser(models.Model):
    user = models.ForeignKey(
        'User', on_delete=models.CASCADE, related_name='User',
    )
    device = models.ForeignKey(
        'Device', on_delete=models.CASCADE, related_name='Device',
    )
    token = models.CharField(max_length=255)


class Device(models.Model):
    name = models.CharField(max_length=255)
    ip = models.CharField(max_length=255)


class ChatUser(models.Model):
    chat = models.ForeignKey(
        'Chat', on_delete=models.CASCADE, related_name='ChatUser_Chat',
    )
    user = models.ForeignKey(
        'User', on_delete=models.CASCADE, related_name='ChatUser_User',
    )

    class UserRoles(models.TextChoices):
        READ = 'rd', 'read'
        READWRITE = 'rw', 'read and write'
        ADMIN = 'ad', 'admin'

    role = models.CharField(
        max_length=2,
        choices=UserRoles.choices,
        default=UserRoles.READWRITE,
    )


class Chat(models.Model):
    name = models.CharField(max_length=255, null=True)
    description = models.TextField('Description', null=True)


class Message(models.Model):
    sender = models.ForeignKey(
        'User', on_delete=models.CASCADE, related_name='sender',
    )
    chat = models.ForeignKey(
        'Chat', on_delete=models.CASCADE, related_name='chat',
        null=True, blank=True,
    )
    timestamp = models.DateTimeField(auto_now_add=True)
    referenced_message = models.ForeignKey(
        'Message', on_delete=models.SET_NULL, related_name='target_message', null=True, blank=True
    )


class MessageUser(models.Model):
    message = models.ForeignKey(
        'Message', on_delete=models.CASCADE, related_name='message',
    )
    receiver = models.ForeignKey(
        'User', on_delete=models.CASCADE, related_name='receiver',
    )
    read_at = models.DateTimeField(null=True)

    class Type(models.TextChoices):
        TEXT = 'txt', 'text'
        FILE = 'fil', 'file'

    type = models.CharField(
        max_length=3,
        choices=Type.choices,
        default=Type.TEXT,
    )


class InviteCode(models.Model):
    user = models.ForeignKey(
        'User', on_delete=models.CASCADE, related_name='InviteCode_User',
    )
    code = models.CharField(max_length=255, unique=True)
    used = models.BooleanField()


class MessageUserText(models.Model):
    message_user = models.ForeignKey(
        'MessageUser', on_delete=models.CASCADE,
        related_name='MessageUser_Text',
    )
    encrypted_message = models.TextField('Encrypted Message')


class MessageUserFiles(models.Model):
    message_user = models.ForeignKey(
        'User', on_delete=models.CASCADE, related_name='MessageUser_Files',
    )
    filename = models.TextField('Filename')


class BlockedChat(models.Model):
    user = models.ForeignKey(
        'User', on_delete=models.CASCADE,
        related_name='User_BlockedChat',
    )
    chat = models.ForeignKey(
        'Chat', on_delete=models.CASCADE,
        related_name='Chat_BlockedChat',
    )


class BlockedUser(models.Model):
    blocking_user = models.ForeignKey(
        'User', on_delete=models.CASCADE,
        related_name='blocking_user_blocked_user',
    )
    blocked_user = models.ForeignKey(
        'User', on_delete=models.CASCADE,
        related_name='blocked_user_blocked_user',
    )
