from __future__ import annotations

from rest_framework import serializers

from main.models import User


class RegisterSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
            public_key=validated_data['public_key'],
            private_key_encrypted=validated_data['private_key_encrypted'],
            salt=validated_data['salt'],
        )

        return user

    class Meta:
        model = User
        fields = (
            'id', 'username', 'password', 'public_key',
            'private_key_encrypted', 'salt',
        )
