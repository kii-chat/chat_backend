from django.urls import include, path
from rest_framework import routers
from main.api import chat
from main.api import group
from main.api import social
from main.api import user

router = routers.DefaultRouter()
router.register(r'register', user.UserCreate)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

user_url_patterns = [
    path('login/', user.login),
    path('me/', user.me),
    path('blockUser/', user.blockUser),
    path('blockedUsers/', user.blockedUser),
    path('unblockUser/', user.unblockUser),
]

social_url_patterns = [
    path('createChatWithUsername/', social.createChatWithUsername),
    path('createChatWithInviteCode/', social.createChatWithInviteCode),
    path('getPublicUsers/', social.getPublicUsers),
    path('setVisibility/', social.setVisibility),
    path('createInviteCode/', social.createInviteCode),
    path('deleteInviteCode/', social.deleteInviteCode),
    path('getInviteCodes/', social.getInviteCodes),
]

group_url_patterns = [
    path('createGroup/', group.createGroup),
    path('addUsersToGroup/', group.addUsersToGroup),
    path('changeUserRoleInChat/', group.changeUserRoleInChat),
]
chat_url_patterns = [
    path('getAllPublicKeysFromChat/', chat.getAllPublicKeysFromChat),
    path('sendTextMessage/', chat.sendTextMessage),
    path('getChatsForUser/', chat.getChatsForUser),
    path('getMessagesFromChat/', chat.getMessagesFromChat),
    path('getInfosFromChat/', chat.getInfosFromChat),
    path('updateChat/', chat.updateChat),
    path('setRead/', chat.setRead),
    path('blockChat/', chat.blockChat),
    path('blockedChats/', chat.blockedChat),
    path('unblockChat/', chat.unblockChat),
    path('leaveChat/', chat.leaveChat),
]
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
] + user_url_patterns + social_url_patterns + group_url_patterns + chat_url_patterns
