# Generated by Django 4.0.4 on 2022-05-12 08:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0009_remove_messageuser_call_room_delete_callroom'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='referenced_message',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='target_message', to='main.message'),
        ),
    ]
