# Generated by Django 4.0.4 on 2022-05-05 10:26
from __future__ import annotations

import django.db.models.deletion
from django.db import migrations
from django.db import models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_callroom'),
    ]

    operations = [
        migrations.AddField(
            model_name='messageuser',
            name='call_room',
            field=models.ForeignKey(
                default=1, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='calls', to='main.callroom',
            ),
        ),
    ]
