from django.urls import re_path
from main import consumers
websocket_urlpatterns = [
    re_path(r'ws/chat/$', consumers.MessageUserConsumer.as_asgi()),
    re_path(r'ws/chatlist/$', consumers.ChatUserConsumer.as_asgi()),
]
