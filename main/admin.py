from __future__ import annotations

from django.contrib import admin

from main.models import Chat
from main.models import ChatUser
from main.models import Device
from main.models import DeviceUser
from main.models import InviteCode
from main.models import Message
from main.models import MessageUser
from main.models import MessageUserFiles
from main.models import MessageUserText
from main.models import User


class UserAdmin(admin.ModelAdmin):
    pass


admin.site.register(User, UserAdmin)


class ChatAdmin(admin.ModelAdmin):
    pass


admin.site.register(Chat, ChatAdmin)


class MessageAdmin(admin.ModelAdmin):
    pass


admin.site.register(Message, MessageAdmin)


class MessageUserAdmin(admin.ModelAdmin):
    pass


admin.site.register(MessageUser, MessageUserAdmin)


class InviteCodeAdmin(admin.ModelAdmin):
    pass


admin.site.register(InviteCode, InviteCodeAdmin)


class DeviceUserAdmin(admin.ModelAdmin):
    pass


admin.site.register(DeviceUser, DeviceUserAdmin)


class DeviceAdmin(admin.ModelAdmin):
    pass


admin.site.register(Device, DeviceAdmin)


class ChatUserAdmin(admin.ModelAdmin):
    pass


admin.site.register(ChatUser, ChatUserAdmin)


class MessageUserTextAdmin(admin.ModelAdmin):
    pass


admin.site.register(MessageUserText, MessageUserTextAdmin)


class MessageUserFilesAdmin(admin.ModelAdmin):
    pass


admin.site.register(MessageUserFiles, MessageUserFilesAdmin)
