#!/bin/bash
commit_message=$(sed '/^\s*$/d' management/tools/bump_messages | shuf -n 1)
file_content=$(head -c 1 ./.bump)

if [[ "$file_content" == "0" ]]
then
    echo -en "1\n" > ./.bump
else
    echo -en "0\n" > ./.bump
fi

echo "$commit_message"

git pull
git add --all
git commit -m "$commit_message"
git push

# echo "do not forget to push"
