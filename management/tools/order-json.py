import sys
import json

jdata = {}
file = sys.argv[1]
try:
    with open(file, 'r') as f:
        jdata = json.loads(f.read())

    with open(file, 'w') as f:
        f.write(json.dumps(jdata, sort_keys=True))
except FileNotFoundError:
    print('order-json.py: file not found')
