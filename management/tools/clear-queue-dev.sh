#!/bin/bash

management/tools/dcdev.sh exec rmq rabbitmqctl stop_app
management/tools/dcdev.sh exec rmq rabbitmqctl reset
management/tools/dcdev.sh exec rmq rabbitmqctl start_app
