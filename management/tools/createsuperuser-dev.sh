#!/bin/bash

management/tools/dcdev.sh exec django python run.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')"
