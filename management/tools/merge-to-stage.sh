#!/bin/bash

currbranch=$(git symbolic-ref --short HEAD)

git checkout stage && \
git merge $currbranch && \
git push && \
git checkout $currbranch
