#!/bin/bash

management/tools/dcprod.sh exec rmq rabbitmqctl stop_app
management/tools/dcprod.sh exec rmq rabbitmqctl reset
management/tools/dcprod.sh exec rmq rabbitmqctl start_app
