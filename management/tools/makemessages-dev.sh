#!/bin/bash
management/tools/dcdev.sh exec django python run.py makemessages --keep-pot -a "$@"
management/tools/dcdev.sh exec django python run.py makemessages --keep-pot -a -e js -d djangojs --ignore=jsi18n "$@"
management/tools/ownme.sh
