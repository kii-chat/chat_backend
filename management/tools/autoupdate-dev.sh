#!/bin/bash
BRANCH=$(git branch --show-current)
watch "git pull origin $BRANCH | ag Already || (management/tools/pull-prod.sh && management/tools/restart-dev.sh)"
