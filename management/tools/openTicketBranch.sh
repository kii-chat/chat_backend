#!/bin/bash

gnb(){
  git checkout -b $1
  git push --set-upstream origin $1
}

if [[ $# -eq 0 ]]; then
    echo "Please enter a ticket id"
    exit 1
fi

branchname=$(curl --header "PRIVATE-TOKEN: glpat-s1MrP7TiHskTMxkEm-ns" "https://git.senbax.cloud/api/v4/projects/20/issues/$1" 2> /dev/null | jq '.title' | head -c -1 | tr '/ \\' '-' | tr -cd '[:alnum:] [:space:] -' | tr '[:space:]' '-')

if [ "$branchname" = "null" ] || [ -z "$branchname" ]; then
  echo "Something went wrong, please panic and call 023158690828"
  exit 1
fi

read -r -p "Creating branch $1-$branchname for ticket $1? [Y/n] " response
response=${response,,}
if [[ $response =~ ^(y| ) ]] || [[ -z $response ]]; then
    gnb "$1-$branchname"
fi
