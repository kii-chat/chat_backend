#!/bin/bash

TAG=$(git branch --show-current)
export TAG
if uname -a | grep -q "arch"; then
  docker compose --env-file .env/local -f dev.yml "$@"
else
  docker-compose --env-file .env/local -f dev.yml "$@"
fi
