#! /bin/bash
envFile=".env/local"

hardrestart(){
  sh management/tools/stop-dev.sh
  sh management/tools/dcdev.sh up -d --remove-orphans
}

startcontainer(){
  sh management/tools/dcdev.sh up -d --remove-orphans "$1"
}

stopcontainer(){
  sh management/tools/dcdev.sh stop "$1"
}

if [[ -z "$1" ]] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
  echo "toggleenv - toggles (adds or removes) a env in .env/local and restarts docker container"
  echo -e "toggleenv.sh \e[4menv\e[0m (not case sensitive)\n"
  echo -e "\e[1mavailable envs:\e[0m"
  echo -e "pgadmin:\t starts or stops pgadmin"
  echo -e "dummycache:\t loads or unloads dummycache (frontdevs hate cache)"
  echo -e "localstatic:\t uses localstatic (collectstatic ~15sec) or AWS s3 static (collectstatic ~30min)"
  echo -e "\nNo env supplied"
  exit 1
fi

env=${1^^}
if grep -q --ignore-case "$env" "$envFile"; then
  echo "removing $env..."
  sed -i "/$env/Id" "$envFile"
  case "$env" in
    "PGADMIN") stopcontainer "pgadmin";;
    *) hardrestart;;
  esac
else
  echo "adding $env..."
  case "$env" in
    "PGADMIN") echo "COMPOSE_PROFILES=pgadmin" >> "$envFile" && startcontainer "pgadmin";;
    *) echo "$env=1" >> "$envFile" && hardrestart;;
  esac
fi
