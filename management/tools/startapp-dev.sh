#!/bin/bash

management/tools/dcdev.sh exec django python run.py startapp "$@"
management/tools/ownme.sh
management/tools/migrate-dev.sh

echo please add "$@" to config.settings.common.INSTALLED_APPS
